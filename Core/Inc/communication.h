/**
 * @file communication.h
 * @author Jakub Kozłowicz <ja.kozlowicz@gmail.com>
 * @brief Header file for comunication with PC module
 * @version 1.0
 * @date 2022-05-14
 *
 * @copyright Copyright (c) 2022 Jakub Kozłowicz
 */

#ifndef COMMUNICATION_H_
#define COMMUNICATION_H_

#define PC_FRAME_LENGTH 48

typedef struct {
	char identifier[2];
	uint8_t state_heater;
	float measured_temp;		//chcecked
	float measured_lux;			//chcecked
	float water_level;			//calculate using Distance
	uint8_t power_source;		//checked
	float baterry_voltage;
	uint16_t crc16;
} PC_FrameData;

/**
 * @brief Initialize communication with PC computer
 * @details Set up identifier and set defaults values
 * @param[in] frame_data Handle to structure data of frame
 * @return Status of setting values
 */
uint8_t PCCOM_Init(PC_FrameData *frame_data);

/**
 * @brief
 * @param[in] frame_data Handle to structure data of frame
 * @param[in] left_motor Speed value of left motor
 * @param[in] right_motor Speed value of right motor
 * @return Status of setting values
 */
uint8_t PPCOM_StateHeater(PC_FrameData *frame_data, uint8_t state_heater);
uint8_t PPCOM_MeasuredTemp(PC_FrameData *frame_data, float measured_temp);
uint8_t PPCOM_MeasuredLight(PC_FrameData *frame_data, float measured_lux);
uint8_t PPCOM_MeasuredWaterLevel(PC_FrameData *frame_data, float water_level);
uint8_t PPCOM_PowerSource(PC_FrameData *frame_data, uint8_t power_source);
uint8_t PPCOM_MeasuredBaterryVoltage(PC_FrameData *frame_data, float baterry_voltage);
/**
 * @brief
 * @param[in] frame_data Handle to structure data of frame
 * @param[out] buffer Buffer to save one frame which will be sent to PC
 * @return Status of saving data to buffer
 */
uint8_t PCCOM_ConstructFrame(PC_FrameData *frame_data,
		char buffer[PC_FRAME_LENGTH], CRC_HandleTypeDef *hcrc );

#endif /* COMMUNICATION_H_ */
