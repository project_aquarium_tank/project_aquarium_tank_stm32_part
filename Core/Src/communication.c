/**
 * @file communication.c
 * @author Jakub Kozłowicz <ja.kozlowicz@gmail.com>
 * @brief Source file for comunication with PC module
 * @version 1.0
 * @date 2022-05-14
 *
 * @copyright Copyright (c) 2022 Jakub Kozłowicz
 */

#include <stdio.h>
#include <string.h>

#include "main.h"

#include "communication.h"

uint8_t PCCOM_Init(PC_FrameData *frame_data) {
	if (sprintf(frame_data->identifier, "%s", "X") != 1) {
		return -1;
	}

	return 0;
}



uint8_t PPCOM_StateHeater(PC_FrameData *frame_data, uint8_t state_heater)
{
	frame_data->state_heater=state_heater;

	return 0;
}

uint8_t PPCOM_MeasuredTemp(PC_FrameData *frame_data, float measured_temp)
{
	frame_data->measured_temp=measured_temp;

	return 0;
}
uint8_t PPCOM_MeasuredLight(PC_FrameData *frame_data, float measured_lux)
{
	frame_data->measured_lux=measured_lux;

	return 0;
}

uint8_t PPCOM_MeasuredWaterLevel(PC_FrameData *frame_data, float water_level)
{
	frame_data->water_level=water_level;

	return 0;
}

uint8_t PPCOM_PowerSource(PC_FrameData *frame_data, uint8_t power_source)
{
	frame_data->power_source=power_source;

	return 0;
}

uint8_t PPCOM_MeasuredBaterryVoltage(PC_FrameData *frame_data, float baterry_voltage)
{
	frame_data->baterry_voltage=baterry_voltage;

	return 0;
}

uint8_t PCCOM_ConstructFrame(PC_FrameData *frame_data,
		char buffer[PC_FRAME_LENGTH], CRC_HandleTypeDef *hcrc) {
	uint16_t crc;
	char crc_sign[5];



sprintf(buffer, "%s %1d %04.2f %010.2f %05.2f %1d %4.2f ",
			frame_data->identifier, frame_data->state_heater,
			frame_data->measured_temp, frame_data->measured_lux,
			frame_data->water_level, frame_data->power_source,
			frame_data->baterry_voltage);


	if(strlen(buffer)!=34)
	{
		return -1;
	}




	crc = HAL_CRC_Calculate(hcrc, (uint32_t*) buffer, strlen(buffer));
	if (sprintf(crc_sign, "%04X", crc) != 4) {
		return -1;
	}


	strcat(buffer, crc_sign);
	//printf("Liczba znakow do CRC16: %d\r\n",strlen(buffer));
	return 0;
}
