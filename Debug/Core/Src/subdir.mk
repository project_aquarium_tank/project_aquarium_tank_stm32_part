################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Core/Src/communication.c \
../Core/Src/main.c \
../Core/Src/stm32l4xx_hal_msp.c \
../Core/Src/stm32l4xx_it.c \
../Core/Src/syscalls.c \
../Core/Src/sysmem.c \
../Core/Src/system_stm32l4xx.c 

OBJS += \
./Core/Src/communication.o \
./Core/Src/main.o \
./Core/Src/stm32l4xx_hal_msp.o \
./Core/Src/stm32l4xx_it.o \
./Core/Src/syscalls.o \
./Core/Src/sysmem.o \
./Core/Src/system_stm32l4xx.o 

C_DEPS += \
./Core/Src/communication.d \
./Core/Src/main.d \
./Core/Src/stm32l4xx_hal_msp.d \
./Core/Src/stm32l4xx_it.d \
./Core/Src/syscalls.d \
./Core/Src/sysmem.d \
./Core/Src/system_stm32l4xx.d 


# Each subdirectory must supply rules for building sources it contributes
Core/Src/%.o Core/Src/%.su: ../Core/Src/%.c Core/Src/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32L476xx -c -I../Core/Inc -I"/home/bartek/Desktop/STM Cube/managment-system-for-aquarium/Drivers/OneWire" -I"/home/bartek/Desktop/STM Cube/managment-system-for-aquarium/Drivers/OneWire/Inc" -I"/home/bartek/Desktop/STM Cube/managment-system-for-aquarium/Drivers/DS18B20" -I"/home/bartek/Desktop/STM Cube/managment-system-for-aquarium/Drivers/BH1750" -I"/home/bartek/Desktop/STM Cube/managment-system-for-aquarium/Drivers/HCSR04" -I"/home/bartek/Desktop/STM Cube/managment-system-for-aquarium/Drivers/DS18B20/Inc" -I"/home/bartek/Desktop/STM Cube/managment-system-for-aquarium/Drivers/BH1750/Inc" -I"/home/bartek/Desktop/STM Cube/managment-system-for-aquarium/Drivers/HCSR04/Inc" -I../Drivers/CMSIS/Include -I../Drivers/CMSIS/Device/ST/STM32L4xx/Include -I../Drivers/STM32L4xx_HAL_Driver/Inc -I../Drivers/STM32L4xx_HAL_Driver/Inc/Legacy -I"/home/bartek/Desktop/STM Cube/managment-system-for-aquarium/Drivers/Heater/Inc" -I"/home/bartek/Desktop/STM Cube/managment-system-for-aquarium/Drivers/Heater/Src" -I"/home/bartek/Desktop/STM Cube/managment-system-for-aquarium/Drivers/LEDs/Inc" -I"/home/bartek/Desktop/STM Cube/managment-system-for-aquarium/Drivers/LEDs/Src" -I"/home/bartek/Desktop/STM Cube/managment-system-for-aquarium/Drivers/PID/Inc" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

clean: clean-Core-2f-Src

clean-Core-2f-Src:
	-$(RM) ./Core/Src/communication.d ./Core/Src/communication.o ./Core/Src/communication.su ./Core/Src/main.d ./Core/Src/main.o ./Core/Src/main.su ./Core/Src/stm32l4xx_hal_msp.d ./Core/Src/stm32l4xx_hal_msp.o ./Core/Src/stm32l4xx_hal_msp.su ./Core/Src/stm32l4xx_it.d ./Core/Src/stm32l4xx_it.o ./Core/Src/stm32l4xx_it.su ./Core/Src/syscalls.d ./Core/Src/syscalls.o ./Core/Src/syscalls.su ./Core/Src/sysmem.d ./Core/Src/sysmem.o ./Core/Src/sysmem.su ./Core/Src/system_stm32l4xx.d ./Core/Src/system_stm32l4xx.o ./Core/Src/system_stm32l4xx.su

.PHONY: clean-Core-2f-Src

