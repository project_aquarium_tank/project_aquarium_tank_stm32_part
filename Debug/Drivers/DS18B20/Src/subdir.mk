################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Drivers/DS18B20/Src/DS18B20.c 

OBJS += \
./Drivers/DS18B20/Src/DS18B20.o 

C_DEPS += \
./Drivers/DS18B20/Src/DS18B20.d 


# Each subdirectory must supply rules for building sources it contributes
Drivers/DS18B20/Src/%.o Drivers/DS18B20/Src/%.su: ../Drivers/DS18B20/Src/%.c Drivers/DS18B20/Src/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32L476xx -c -I../Core/Inc -I"/home/bartek/Desktop/STM Cube/managment-system-for-aquarium/Drivers/OneWire" -I"/home/bartek/Desktop/STM Cube/managment-system-for-aquarium/Drivers/OneWire/Inc" -I"/home/bartek/Desktop/STM Cube/managment-system-for-aquarium/Drivers/DS18B20" -I"/home/bartek/Desktop/STM Cube/managment-system-for-aquarium/Drivers/BH1750" -I"/home/bartek/Desktop/STM Cube/managment-system-for-aquarium/Drivers/HCSR04" -I"/home/bartek/Desktop/STM Cube/managment-system-for-aquarium/Drivers/DS18B20/Inc" -I"/home/bartek/Desktop/STM Cube/managment-system-for-aquarium/Drivers/BH1750/Inc" -I"/home/bartek/Desktop/STM Cube/managment-system-for-aquarium/Drivers/HCSR04/Inc" -I../Drivers/CMSIS/Include -I../Drivers/CMSIS/Device/ST/STM32L4xx/Include -I../Drivers/STM32L4xx_HAL_Driver/Inc -I../Drivers/STM32L4xx_HAL_Driver/Inc/Legacy -I"/home/bartek/Desktop/STM Cube/managment-system-for-aquarium/Drivers/Heater/Inc" -I"/home/bartek/Desktop/STM Cube/managment-system-for-aquarium/Drivers/Heater/Src" -I"/home/bartek/Desktop/STM Cube/managment-system-for-aquarium/Drivers/LEDs/Inc" -I"/home/bartek/Desktop/STM Cube/managment-system-for-aquarium/Drivers/LEDs/Src" -I"/home/bartek/Desktop/STM Cube/managment-system-for-aquarium/Drivers/PID/Inc" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

clean: clean-Drivers-2f-DS18B20-2f-Src

clean-Drivers-2f-DS18B20-2f-Src:
	-$(RM) ./Drivers/DS18B20/Src/DS18B20.d ./Drivers/DS18B20/Src/DS18B20.o ./Drivers/DS18B20/Src/DS18B20.su

.PHONY: clean-Drivers-2f-DS18B20-2f-Src

