/*
 * HCSR04.h
 *
 *  Created on: Aug 27, 2022
 *      Author: barte
 */

#ifndef HCSR04_INC_HCSR04_H_
#define HCSR04_INC_HCSR04_H_

#define HCSR04_HIGH_PRECISION

typedef enum{
	HCSR04_OK 	 = 0,
	HCSR04_ERROR = 1
} HCSR04_STATUS;

HCSR04_STATUS HCSR04_Init(TIM_HandleTypeDef *htim);
#ifdef HCSR04_HIGH_PRECISION
HCSR04_STATUS HCSR04_Read(float *Result);
#else
HCSR04_STATUS HCSR04_Read(uint16_t *Result);
#endif


#endif /* HCSR04_INC_HCSR04_H_ */
