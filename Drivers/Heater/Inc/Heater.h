/*
 * Heater.h
 *
 *  Created on: Oct 8, 2022
 *      Author: bartek
 */

#ifndef HEATER_INC_HEATER_H_
#define HEATER_INC_HEATER_H_

#include "stm32l4xx_hal.h"
#include <math.h>

#define HOUR_1 12
#define HOUR_2 16
#define HOUR_3 23
#define HOUR_4 7

#define L_MARGIN 1
#define H_MARGIN 1
#define HYSTERESIS 0.5

#define FACTOR 0.014
#define MIN_TEMP 18.0


int heater_auto(uint8_t hour, GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin, float set_temp);

int heater_manual( GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin, GPIO_PinState PinState);

#endif /* HEATER_INC_HEATER_H_ */
