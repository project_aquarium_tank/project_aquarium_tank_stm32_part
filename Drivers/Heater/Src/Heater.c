/*
 * Heater.c
 *
 *  Created on: Oct 8, 2022
 *      Author: bartek
 */
#include "Heater.h"
#include "DS18B20.h"


int heater_auto(uint8_t act_hour, GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin, float set_temp)
{
	float temperature;
	ds18b20_start_measure(NULL);
	temperature = ds18b20_get_temp(NULL);

	//printf("\r\nAktualna temp. wynosi %04.2f \r\n", temperature);

	if(temperature<= set_temp-L_MARGIN)
	{
		//printf("Grzanie w zakresie ponizej <1 stopnia.\r\n");
		HAL_GPIO_WritePin(GPIOx, GPIO_Pin, GPIO_PIN_SET);
		return GPIO_PIN_SET;
	}
	else if(((act_hour>=HOUR_1&& act_hour<=HOUR_2) || (act_hour>=HOUR_3&& act_hour<=HOUR_4))&&(temperature<=set_temp+H_MARGIN))
	{
		//printf("Grzanie podczas taryfy w zakresie ponizej 1 stopnia do 1.5 stponia powyzej. \r\n");
		HAL_GPIO_WritePin(GPIOx, GPIO_Pin, GPIO_PIN_SET);
		return GPIO_PIN_SET;
	}
	else if(temperature>=set_temp+H_MARGIN+HYSTERESIS)
	{
		//printf("Wylaczenie grzalki w taryfie.\r\n");
		HAL_GPIO_WritePin(GPIOx, GPIO_Pin, GPIO_PIN_RESET);
		return GPIO_PIN_RESET;
	}
	else if(temperature>=set_temp-L_MARGIN+HYSTERESIS)
	{
		//printf("Wylaczenie grzalki poza taryfa.\r\n");
		HAL_GPIO_WritePin(GPIOx, GPIO_Pin, GPIO_PIN_RESET);
		return GPIO_PIN_RESET;
	}else
	{
		return -1;
	}

}



int heater_manual( GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin, GPIO_PinState PinState)
{

//	ds18b20_start_measure(NULL);
//	float temperature = ds18b20_get_temp(NULL);

	//printf("\r\nAktualna temp. wynosi %04.2f \r\n", temperature);
	//printf("Grzalka pracuje w trybie manualnym. \r\n");
	HAL_GPIO_WritePin(GPIOx, GPIO_Pin, PinState);

	return PinState;
}
