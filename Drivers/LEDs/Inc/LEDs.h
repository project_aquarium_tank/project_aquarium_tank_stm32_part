/*
 * LEDs.h
 *
 *  Created on: Oct 16, 2022
 *      Author: bartek
 */

#ifndef LEDS_INC_LEDS_H_
#define LEDS_INC_LEDS_H_

#include "stm32l4xx_hal.h"
#include "stm32l4xx_hal_def.h"
#include "PID.h"
//
//#define FACTOR_LED 8.5568
//#define CONSTANT_LED 39.5
//#define HYSTERESIS_LED 10.0


#define LED_Kp					1.5
#define LED_Ki					0.8
#define LED_Kd					0.5
#define LED_ANTI_WINDUP			20

#define MAX_LUX 10000
#define MIN_LUX 0


typedef struct LED
{
	GPIO_TypeDef* GPIOx;
	uint16_t GPIO_Pin;

	TIM_HandleTypeDef* timer;

	int set_lx;
	int measured_lx;

	uint8_t act_hour;
	uint8_t act_min;

	uint8_t on_hour;
	uint8_t on_min;

	uint8_t off_hour;
	uint8_t off_min;

	pid_str pid_controller;

	uint16_t actual_PWM;
}led;


void led_init(led* LED, GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin, TIM_HandleTypeDef* timer);

void led_set_brightness(led* LED, int set_brightness);

void leds_auto(led* LED);

void leds_manual( GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin, TIM_HandleTypeDef* timer, uint16_t );

void led_sunset(led* LED);

void led_sunrise(led* LED);

void led_controll(led* LED);


#endif /* LEDS_INC_LEDS_H_ */
