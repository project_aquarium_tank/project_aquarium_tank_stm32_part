/*
 * LEDs.c
 *
 *  Created on: Oct 16, 2022
 *      Author: bartek
 */
#include "LEDs.h"
#include <math.h>


void led_init(led* LED, GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin, TIM_HandleTypeDef* timer)
{
	LED->timer=timer;
	LED->GPIOx=GPIOx;
	LED->GPIO_Pin=GPIO_Pin;
	LED->actual_PWM=0;
	LED->measured_lx=0;
	LED->set_lx=0;
}

void led_set_brightness(led* LED, int set_brightness)
{
	if(set_brightness>=MIN_LUX && set_brightness<=MAX_LUX)
	{
		if(set_brightness!= LED->set_lx)
		{
			pid_reset(&(LED->pid_controller));
		}

		LED->set_lx=set_brightness;
	}
}


void leds_manual( GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin, TIM_HandleTypeDef* timer, uint16_t pow_led)
{
//	float value = pow_led*99.99;
//	value=round(value);

	__HAL_TIM_SET_COMPARE(timer, TIM_CHANNEL_2, pow_led);
	//printf("Led manual, value 0-9999: %.2f \r\n",value);
}


void leds_auto(led* LED)
{

	int output = pid_calculate(&(LED->pid_controller), LED->set_lx, LED->measured_lx);

		LED->actual_PWM+=output;
		__HAL_TIM_SET_COMPARE(LED->timer, TIM_CHANNEL_2, LED->actual_PWM);


}



void led_controll(led* LED)
{
	if(LED->off_hour==LED->act_hour && LED->off_min==LED->act_min )
	{
		__HAL_TIM_SET_COMPARE(LED->timer, TIM_CHANNEL_2, 0);
	}
	else if(LED->on_hour==LED->act_hour && LED->on_min==LED->act_min )
	{
		int output = pid_calculate(&(LED->pid_controller), LED->set_lx, LED->measured_lx);

		LED->actual_PWM+=output;
		__HAL_TIM_SET_COMPARE(LED->timer, TIM_CHANNEL_2, LED->actual_PWM);
	}
}


