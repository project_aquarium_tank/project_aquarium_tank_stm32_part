# Project_aquarium_tank_STM32_part


## Description
This project is part of the engineering work. The created device is a controller designed to operate devices in an aquarium. It controls the operation of the heater, lighting and filter. A DS18B20 sensor was used to control the temperature. Temeperature control is done binary using a hysteresis loop. A BH1750 sensor and a PID controller were used to regulate lighting intensity. The controller additionally communicates via a serial port with a Raspberry Pi responsible for displaying the GUI.

## Authors and acknowledgment
- Bartosz Wroński

## License
Copyright (c) 2022 Bartosz Wroński
MIT License

